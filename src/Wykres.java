
import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * Servlet implementation class Wykres
 */
@WebServlet("/Wykres/*")
@MultipartConfig(fileSizeThreshold=1024*1024*10,    // 10 MB
maxFileSize=1024*1024*10,          // 10 MB
maxRequestSize=1024*1024*10)      // 10 MB

public class Wykres extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//nazwa folderu w ktorym maja byc zapisywane dane
	private static final String UPLOAD_DIR = "dane";	//nazwa folderu

	// Wyznacza sciezke do miejsca na serwerze gdzie znajduje sie aplikacja
	String applicationPath;
	// Generuje sciezke do folderu w ktorym maja byc umieszczone pliki
	String uploadFilePath;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Wykres() {
		//super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * wywolywane bezparametrowo, generuje podstawowy wyglad strony i laduje ajax
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Wyznacza sciezke do miejsca na serwerze gdzie znajduje sie aplikacja
		applicationPath = request.getServletContext().getRealPath("");
		// Generuje sciezke do folderu w ktorym maja byc umieszczone pliki
		uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;
		// Pobieranie listy plików
		File folder = new File(uploadFilePath);
		File[] listOfFiles = folder.listFiles();
		String [] files = new String[listOfFiles.length];
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				files[i] = listOfFiles[i].getName();
			}
		}
		//obsługa Get, w zależności od wartości parametru Name, zwraca listę plików lub wykres
		JSONObject json = new JSONObject();
		if(request.getParameter("Name")!=null){
			if(request.getParameter("Name").equals("lista")){
				String list = "masz liste";
				try {
					json.put("lista", files);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				ArrayList<Integer> ping = ShowChartFromFile(request.getParameter("Name")).ping;
				ArrayList<String> time = ShowChartFromFile(request.getParameter("Name")).time;
				JSONArray p = new JSONArray(ping);
				JSONArray t = new JSONArray(time);
				try {
					json.put("ping", p);
					json.put("time", t);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

		//ustawiene nagłówka umożliwiające zawołania GET/POST
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type,Authorization");
		response.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");

		response.setContentType("application/json");;  // Set content type of the response so that jQuery knows what it can expect.
		response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
		response.getWriter().print(json);	// zwrot zawartości json 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String text = "Plik został wczytany";	// wiadomośc wyświetlana przy wczytaniu pliku
		//ustawiene nagłówka umożliwiające zawołania GET/POST
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type,Authorization");
		response.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");

		response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
		response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
		response.getWriter().write(text); // wypisanie wiadomości
		// Wyznacza sciezke do miejsca na serwerze gdzie znajduje sie aplikacja
		applicationPath = request.getServletContext().getRealPath("");
		// Generuje sciezke do folderu w ktorym maja byc umieszczone pliki
		uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;


		//tmp utworzenie uchwytu umożliwiające przesłanie treści
		PrintWriter out = response.getWriter();

		// creates the save directory if it does not exists
		// tworzy ścieżkę dla nowych plików
		File fileSaveDir = new File(uploadFilePath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
			out.println("utworzono");
		}
		//utworzenie obiektu dla nowego pliku
		String fileName = null;
		//Get all the parts from request and write it to the file on server
		//zapis pliku na serwer
		for (Part part : request.getParts()) {
			fileName = getSubmittedFileName(part);
			part.write(uploadFilePath + File.separator + fileName);
		}

		//request.setAttribute("message", fileName + " File uploaded successfully!");
		//getServletContext().getRequestDispatcher("/response.jsp").forward(
		//        request, response);


		//ustawiene nagłówka umożliwiające zawołania GET/POST
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type,Authorization");
		response.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
		response.setContentType("text/html");

		// New location to be redirected
		response.sendRedirect(request.getHeader("referer"));

		/*String site = new String("http://127.0.0.1");

		        response.setStatus(response.SC_MOVED_TEMPORARILY);
		        response.setHeader("Location", site);  */


	}

	/**
	 * Utility method to get file name from HTTP header content-disposition
	 */
	private static String getSubmittedFileName(Part part) {
		for (String cd : part.getHeader("content-disposition").split(";")) {
			if (cd.trim().startsWith("filename")) {
				String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
				return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
			}
		}
		return null;
	}
	// funkcja parsująca plik tekstowy na tablicę liczb
	private data ShowChartFromFile (String FileName) throws IOException{

		File file = new File(uploadFilePath+File.separator+FileName);
		
        
		FileReader fileReader = new FileReader(file);
        BufferedReader buffReader = new BufferedReader(fileReader);

        String filename = file.getName().replace("_netlog.txt", "");
        filename = filename.split("T")[0];
        String buffer = new String();

        ArrayList<Integer> ping = new ArrayList<Integer>();
        ArrayList<String> time = new ArrayList<String>();
        int x=0;
        while( (buffer = buffReader.readLine() ) != null){
            if (!buffer.contains(filename)&&buffer.contains(",")){
                ping.add(Integer.parseInt(buffer.split(",")[8])) ;
                if(x%10==0){
                	long millis = Integer.parseInt(buffer.split(",")[0]);
                    long second = (millis / 1000) % 60;
                    long minute = (millis / (1000 * 60)) % 60;
                    long hour = (millis / (1000 * 60 * 60)) % 24;

                    String t = String.format("%02d:%02d:%02d", hour, minute, second);
                    time.add(t);
                }else{
                	time.add("");
                }
                
            }
            x++;
        }
        buffReader.close();
		return new data(time,ping);
	}
	public class data{
		ArrayList<String> time = new ArrayList<String>();
		ArrayList<Integer> ping = new ArrayList<Integer>();
		
		public data (ArrayList<String> time, ArrayList<Integer> ping){
			this.time = time;
			this.ping = ping;
		}
		
	}
}